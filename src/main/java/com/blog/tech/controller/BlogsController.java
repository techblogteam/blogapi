package com.blog.tech.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.blog.tech.dto.ApiResponse;
import com.blog.tech.service.BlogsService;
import com.blog.tech.model.TblBlogs;

@ComponentScan
@RequestMapping("/blogs")
@RestController
public class BlogsController {
	
	@Autowired
	private BlogsService blogsService;
	
	@GetMapping("/getall")
	public ApiResponse<List<TblBlogs>> getAll(@RequestHeader(value = "Authorization") String token){
		return blogsService.getAllBlogs(token);
	}
}
