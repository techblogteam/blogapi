package com.blog.tech.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.blog.tech.config.JwtTokenUtil;
import com.blog.tech.dto.ApiResponse;
import com.blog.tech.dto.AuthToken;
import com.blog.tech.dto.LoginUser;
import com.blog.tech.model.TblUser;
import com.blog.tech.service.UserService;

@ComponentScan
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/auth")
@RestController
public class AuthenticationController {

	@Autowired
	private UserService userService;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@GetMapping("/healthcheck")
	public String healthcheck() {
		return "success";
	}

	@PostMapping("/login")
	public ApiResponse<AuthToken> login(@RequestBody LoginUser loginUser) {
		final String token = jwtTokenUtil.generateToken(loginUser.getUsername());
		return userService.login(loginUser, token, authenticationManager);
	}

	@GetMapping("/getusers")
	public ApiResponse<List<TblUser>> getallusers() {
		return new ApiResponse<>(HttpStatus.OK.value(), "Success", userService.findAll());
	}

}
