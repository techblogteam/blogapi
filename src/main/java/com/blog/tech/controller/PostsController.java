package com.blog.tech.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.blog.tech.dto.ApiResponse;
import com.blog.tech.dto.PostsDto;
import com.blog.tech.model.TblPost;
import com.blog.tech.service.PostsService;

@ComponentScan
@RequestMapping("/posts")
@RestController
public class PostsController {
	
	@Autowired
	private PostsService postService;
	
	@GetMapping("/getall")
	public ApiResponse<PostsDto> getAllPosts(@RequestHeader(value = "Authorization") String token){
		return postService.getAllPosts(token);
	}
}
