package com.blog.tech.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.blog.tech.dto.ApiResponse;
import com.blog.tech.model.TblCategories;
import com.blog.tech.service.CategoriesService;

@ComponentScan
@RequestMapping("/categories")
@RestController
public class CategoriesController {

	@Autowired
	CategoriesService categoriesService;

	@GetMapping("/healthcheck")
	public String TestRequest() {
		return "Success";
	}

	@GetMapping("/getcategories")
	public ApiResponse<List<TblCategories>> getAll(@RequestHeader(value = "Authorization") String token) {
		return categoriesService.getAllCategories(token);
	}

	@PostMapping("/savecategory")
	public ApiResponse<TblCategories> saveCategory(@RequestBody TblCategories category,
			@RequestHeader(value = "Authorization") String token) {
		return categoriesService.saveCategory(category, token);
	}

	@PostMapping("/saveall")
	public ApiResponse<List<TblCategories>> saveAll(@RequestBody List<TblCategories> categoriesList,
			@RequestHeader(value = "Authorization") String token) {
		return categoriesService.saveAllCategories(categoriesList, token);
	}
}
